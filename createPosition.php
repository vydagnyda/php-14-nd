<?php
require_once 'functions.php';
//todo - check if id exists
$pdo = getConnection();

?>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h1>Nauja pozicija</h1>
        <br>
        <form method="POST">
            <div  class="labels">
                Pavadinimas: <input type="text" name="name">
            </div>
            <br>
            <div class="labels">
                Bazinis atlyginimas: <input type="text" name="base_salary">
            </div>
            <br>
            <input type="submit" value="Patvirtinti">
        </form>
    </div>
</body>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //tod o- field validation
    $result = createPosition($pdo, $_POST['name'], (int) $_POST['base_salary']);
    if ($result) {
        header('Location:index.php');
        exit();
    }
}
?>
</html>
