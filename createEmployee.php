<?php require_once 'functions.php';

    $pdo = getConnection();
    $positions = getAllPositions($pdo);
    
?>
<html>
<head>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">   
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
 
</head>

<body>
<div class="container">
    <h1 text-align="center">Naujas darbuotojas</h1>
    <br>
    <form action="" method="POST">
        <div class="labels">
            <label>Vardas</label>
            <input type="text" name="name"/>
        </div>
        <div class="labels">
            <label>Pavardė</label>
            <input type="text" name="surname"/>
        </div>
        <div class="labels">
            <label>Lytis</label>
            <select name="gender">
                <option value="1">Vyras</option>
                <option value="2">Moteris</option>
            </select>
        </div>
        <div class="labels">
            <label>Telefonas</label>
            <input type="text" name="phone"/>
        </div>
        <div class="labels">
            <label>Gimimo data</label>
            <input type="text" name="birthday" class="datepicker"/>
        </div>
        <div class="labels">
            <label>Išsilavinimas</label>
            <input type="text" name="education"/>
        </div>
        <div class="labels">
            <label>Atlyginimas</label>
            <input type="text" name="salary"/>
        </div>
        <div class="labels">
            <label>Įdarbinimo tipas</label>
            <select name="idarbinimo_tipas">
                <option value="1">Kontraktas</option>
                <option value="2">Autorinis</option>
            </select>   
        </div>
        <div class="labels">
            <label>Pareigos id</label>
            <select name="pareigos_id">
            <?php
            foreach ($positions as $position) { ?>
                <option value="<?php echo $position['id']; ?>"><?php echo $position['name']; ?></option>
            <?php }?>  
            </select>      
        </div>
        <br>
        <div>
            <input type="submit" value="Patvirtinti">
        </div>        

</div>
</form>      
<script>
$( function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
</script>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        //todo - field validation
        $result = createEmployee(
            $pdo,
            $_POST['name'],
            $_POST['surname'],
            $_POST['gender'],
            $_POST['phone'],
            $_POST['birthday'],
            $_POST['education'],
            (int) $_POST['salary'],
            (int) $_POST['idarbinimo_tipas'],
            (int) $_POST['pareigos_id']
        );
    
        if ($result) {
            header('Location:index.php');
            exit();
        }
    }?>

</body>
</html>