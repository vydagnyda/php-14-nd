<?php require_once 'functions.php';
?>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>

<body>
  <div class="container">
    <h1 text-align="center">Įmonės darbuotojai</h1>
    <br>
    <table class="table">     
        <tr>
          <th scope="col">#</th>
          <th scope="col">Darbuotojas</th>
          <th scope="col">Gimimo data</th>
          <th scope="col">Atlyginimas</th>
          <th scope="col">Veiksmai</th>
        </tr>            
        <?php 
        $pdo = getConnection();
        $resultEmployees = getEmployeesList($pdo);
        $i = 0;
        foreach ($resultEmployees as $darbuotojas) {
            $i++;?>
            <tr>                   
                <th scope="row"><?php echo $i;?></th>             
                <td scope="row"><a href="employee.php?id=<?php echo $darbuotojas['id'];?> "> <?php echo $darbuotojas['name'] . ' ' . $darbuotojas['surname'];?></a></td>
                <td scope="row"> <?php echo $darbuotojas['birthday'];?></td> 
                <td scope="row"> <?php echo $darbuotojas['salary'];?></td>    
                <td>
                <div class="buttons">
                    <form action="deleteEmployee.php" method="POST">
                        <input type="submit" value="Trinti">
                        <input type="hidden" value="<?php echo $darbuotojas['id']; ?>" name="id">
                    </form>                                 
            </div>
            </td>   
            </tr>
        <?php }?>      
    </table>
    <br>
    <form action="createEmployee.php" method="POST">
        <input type="submit" value="Pridėti naują darbuotoją">                     
    </form>
    <br>
    <h1 text-align="center">Pareigos</h1>
    <table class="table">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Pareigos</th>
          <th scope="col">Bazinis atlyginimas</th>
          <th scope="col">Darbuotojų skaičius</th>
          <th scope="col">Veiksmai</th>
        </tr>     
        <?php 
        $resultPositions = getPositionsList($pdo);  
        $positionsCount = getPositionsCount($pdo);           
        $i = 0;
        foreach ($resultPositions as $position) {          
            $i++;?>
            <tr>           
            <th scope="row"> <?php echo $i; ?></th>
            <td scope="row"><a href="positions.php?id=<?php echo $position['id']; ?> "><?php echo $position['name'];?></a></td>              
            <td scope="row"> <?php echo $position['base_salary'];?></td>          
            <td scope="row"><?php 
            if(isset($positionsCount[$position['id']])) {
                echo $positionsCount[$position['id']];
            } else {
                echo '0';
             } ?></td>   
            <td>
            <div class="buttons">
                <form action="deletePosition.php" method="POST">
                    <input type="submit" value="Trinti">
                    <input type="hidden" value="<?php echo $position['id']; ?>" name="id">
                    <input type="hidden" value="<?php echo $positionsCount[$position['id']]; ?>" name="positions_count">
                </form>  
                <form action="editPosition.php" method="POST">
                    <input type="submit" value="Atnaujinti">
                    <input type="hidden" value="<?php echo $position['id']; ?>" name="id">               
                </form>             
            </div>
            </td>  
            </tr>        
        <?php }?>    
     
    </table>    
    <form action="createPosition.php" method="POST">
        <input type="submit" value="Pridėti poziciją">                     
    </form>
  </div>

</body>
</html>