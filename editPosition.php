<?php
require_once 'functions.php';
$positionId = $_GET['id'];
//todo - check if id exists
$pdo = getConnection();
$position = getPosition($pdo, !empty($_POST['id']) ? (int) $_POST['id'] : (int) $_GET['id']);
?>

<html>
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <h1>Pozicijos redagavimas - <?php echo $position['name']; ?></h1>
        <br>
        <form method="POST">
            <div class="labels">
                Pavadinimas: <input type="text" name="name" value="<?php echo $position['name']; ?>">
            </div>
            <br>
            <div class="labels">
                Bazinis atlyginimas: <input type="text" name="base_salary" value="<?php echo $position['base_salary']; ?>">
            </div>
            <br>
            <input name="id" type="hidden" value="<?php echo $position['id']; ?>" />
            <input type="submit" value="Patvirtinti">
        </form>
    </div>
</body>
</html>

<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //todo field validation
        updatePosition($pdo, (int) $_POST['id'], $_POST['name'], (int) $_POST['base_salary']);    
    }
    //todo flash message
    header('Location:index.php');
    exit();    
?>

