<?php
require_once 'functions.php';

$pdo = getConnection();
$employee = getEmployee($pdo, !empty($_POST['id']) ? (int) $_POST['id'] : (int) $_GET['id']);
$positions = getAllPositions($pdo);

?>
<html>
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>

<div class="container">
<h1>Darbuotojo redagavimas - <?php echo $employee['name']; ?> <?php echo $employee['surname']; ?></h1>
<br>
<form method="POST">
    <input type="hidden" value="<?php echo $employee['id']; ?>" name="id">
    <div>
        <label >Vardas</label>
        <input type="text" name="name" value="<?php echo $employee['name']; ?>">       
    </div>
    <div>
        <label>Pavardė</label>
        <input type="text" name="surname" value="<?php echo $employee['surname']; ?>" />
    </div>
    <div>
        <label>Lytis</label>
        <select name="gender">
            <option value="1">Vyras</option>
            <option value="2">Moteris</option>
        </select>
    </div>
    <div>
        <label>Telefonas</label>
        <input type="text" name="phone" value="<?php echo $employee['phone']; ?>" />
    </div>
    <div>
        <label>Gimimo data</label>
        <input type="text" name="birthday" class="datepicker" value="<?php echo substr($employee['birthday'], 0, 10); ?>"/>
    </div>
    <div>
        <label>Išsilavinimas</label>
        <input type="text" name="education" value="<?php echo $employee['education']; ?>" />
    </div>
    <div>
        <label>Atlyginimas</label>
        <input type="text" name="salary" value="<?php echo $employee['salary']; ?>" />
    </div>
    <div>
        <label>Įdarbinimo tipas</label>
        <select name="idarbinimo_tipas">
            <option value="1">Paprastas</option>
            <option value="2">Kontraktas</option>
        </select>
    </div>
    <div>
        <label>Pareigos id</label>
        <select name="pareigos_id">
        <?php foreach ($positions as $position) {?>
            <option value="<?php echo $position['id']; ?>"><?php echo $position['name']; ?></option>
        <?php }?>
        </select>
    </div>
    <br>
    <div>
        <input type="submit" value="Patvirtinti">       
    </div>
</div>
</form>

<script>
    $( function() {
        $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });
</script>


</html>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //todo - field validation    
    $result = updateEmployee(
        $pdo,
        (int) $_POST['id'],
        $_POST['name'],
        $_POST['surname'],
        $_POST['gender'],
        $_POST['phone'],
        $_POST['birthday'],
        $_POST['education'],
        (int) $_POST['salary'],
        (int) $_POST['idarbinimo_tipas'],
        (int) $_POST['pareigos_id']
    );  

}
//todo flash message
if ($result) {
header('Location:index.php');
exit();
}
?>
