<?php require_once 'functions.php';?>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <div class="container">
    <a href="index.php">Atgal</a>
    <?php 
    $pdo = getConnection();  
    $id = (int) $_GET['id'];  
    $resultEmployeesByPosition = getEmployeesByPosition($pdo, $id);
    $positionName = getPositionName($pdo, $id);?>
    <h1 text-align="center">Darbuotojai pagal poziciją <?php echo $positionName['name']?></h1>
    <br>
    <table class="table">     
        <tr>
            <th>Vardas ir pavardė</th>
            <th>Gimimo data</th>
            <th>Išsilavinimas</th>
            <th>Atlyginimas</th>
            <th>Telefonas</th>
        </tr>    
        <?php foreach ($resultEmployeesByPosition as $employeeByPosition) {?>         
        <tr>          
            <td scope="col"><a href="employee.php?id=<?php echo $employeeByPosition['id']; ?>"> <?php echo $employeeByPosition['name'] . ' ' . $employeeByPosition['surname'];?></a></td>  
            <td scope="col"> <?php echo $employeeByPosition['birthday'];?></td>        
            <td scope="col"> <?php echo $employeeByPosition['education'];?></td>      
            <td scope="col"> <?php echo $employeeByPosition['salary'];?></td>        
            <td scope="col"> <?php echo $employeeByPosition['phone'];?></td>
        </tr>              
        <?php } ?>    
        <br>   
    </table>   
</body>
</html>