<?php declare (strict_types = 1);

const HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DATABASE = 'baltic_talents';

const NPD = 149;
const INCOME_TAX_PERCENT = 0.15;
const HEALTH_INSURANCE_PERCENT = 0.06;
const SOCIAL_INSURANCE_PERCENT = 0.06;
const SODRA_PERCENT = 0.3098;
const WARANTY_FUND_PERCENT = 0.002;


function getConnection(): PDO 
{
    return new PDO('mysql:host=' . HOST .';dbname=' . DATABASE .';charset=utf8mb4', DB_USER, DB_PASSWORD);
}

function getEmployeesList(PDO $pdo): array
{
    $query = $pdo->prepare('SELECT id, name, surname, birthday, salary FROM darbuotojai');
    $query->execute();
    return $query->fetchAll();
}

function getPositionsList(PDO $pdo): array
{
    $query = $pdo->prepare('SELECT id, name, base_salary FROM pareigos');
    $query->execute();
    return $query->fetchAll();
}

function getEmployee(PDO $pdo, int $id): array
{
    $query = $pdo->prepare('SELECT id, name, surname, education, salary, phone, birthday, pareigos_id, idarbinimo_tipas FROM darbuotojai WHERE id =:id');
    $query->execute(['id' => $id]);
    return $query->fetch();
   
}


function getEmployeesByPosition($pdo, $position_id): array
{
    $query = $pdo->prepare('SELECT id, name, surname, birthday, education, salary, phone FROM darbuotojai WHERE pareigos_id =:id');
    $query->execute(['id' => $position_id]);
    return $query->fetchAll();
}

function getPositionName($pdo, $position_id) 
{    
    $query = $pdo->prepare('SELECT id, name FROM pareigos WHERE id =:id');
    $query->execute(['id' => $position_id]);
    return $query->fetch();
}

function getPositionsCount($pdo):array
{
    $query = $pdo->prepare('SELECT pareigos_id, COUNT(*) FROM darbuotojai GROUP BY pareigos_id');
    $query->execute();
    return $query->fetchAll(PDO::FETCH_KEY_PAIR); 
}

function getTaxesForEmployee($salary): array
{
    $incomeTax = ($salary - NPD) * INCOME_TAX_PERCENT;
    $healthSecurityTax = $salary * HEALTH_INSURANCE_PERCENT;
    $socialSecurityTax = $salary * SOCIAL_INSURANCE_PERCENT;
    $salaryAfterTaxes = $salary - $incomeTax - $healthSecurityTax - $socialSecurityTax;

    $SODRA = $salary * SODRA_PERCENT;
    $fund = $salary * WARANTY_FUND_PERCENT;
    $totalTaxesForEmployee = $salary + $SODRA + $fund;

    return ['income_tax' => $incomeTax,
    'health_security_tax' => $healthSecurityTax,
    'social_security_tax' => $socialSecurityTax,
    'salary_after_taxes' => $salaryAfterTaxes,
    'SODRA' => $SODRA,
    'fund' => $fund,
    'total' => $totalTaxesForEmployee,
    ];
}

function getStatisticsByEducation(PDO $pdo): array
{
    $query = $pdo->prepare('SELECT education, COUNT(*) employee_count, AVG(salary) average_salary FROM darbuotojai GROUP BY education');
    $query->execute();
    return $query->fetchAll();
}


function getTotalEmployeeCount(PDO $pdo): int
{
    $query = $pdo->prepare('SELECT COUNT(*) employee_count FROM darbuotojai');
    $query->execute();
    $result = $query->fetch();

    return (int) $result['employee_count'];
}

function getStatisticsByGender(PDO $pdo)
{
    $query = $pdo->prepare('SELECT gender, COUNT(*) employee_count FROM darbuotojai GROUP BY gender');
    $query->execute();

    return $query->fetchAll();
}

function getAllPositions(PDO $pdo): array 
{
    $query = $pdo->prepare('SELECT id, name, base_salary FROM pareigos');
    $query->execute();
    return $query->fetchAll(); 
}

function createEmployee(PDO $pdo, string $name, string $surname, string $gender, string $phone, string $birthday, string $education, int $salary, int $idarbinimoTipas, int $pareigosId): bool
{
    $sql = 'INSERT into darbuotojai(name, surname, gender, phone, birthday, education, salary, idarbinimo_tipas, pareigos_id)
     VALUES (:name, :surname, :gender, :phone, :birthday, :education, :salary,  :idarbinimo_tipas, :pareigos_id)';

    $query = $pdo->prepare($sql);
    return $query->execute([
        'name' => $name,
        'surname' => $surname,
        'gender' => $gender,
        'phone' => $phone,
        'birthday' => $birthday,
        'education' => $education,
        'salary' => $salary,
        'idarbinimo_tipas' => $idarbinimoTipas,
        'pareigos_id' => $pareigosId,
    ]);
}

function getPosition(PDO $pdo, int $id)
{
    $query = $pdo->prepare('SELECT id, name, base_salary FROM pareigos WHERE id=:id');
    $query->execute(['id' => $id]);
    return $query->fetch(); 
}


function updatePosition(PDO $pdo, int $id, string $name, int $baseSalary)
{
    $sql = 'UPDATE pareigos SET name=:name, base_salary=:base_salary WHERE id=:id';
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id, 'name' => $name, 'base_salary' => $baseSalary]);
}

function updateEmployee(PDO $pdo, int $id, string $name, string $surname, string $gender, string $phone, string $birthday, string $education, int $salary, int $idarbinimoTipas, int $pareigosId): bool
{
    $sql = 'UPDATE darbuotojai
    SET name=:name,
        surname=:surname,
        gender=:gender,
        phone=:phone,
        birthday=:birthday,
        education=:education,
        salary=:salary,
        idarbinimo_tipas=:idarbinimo_tipas,
        pareigos_id=:pareigos_id
    WHERE id=:id';

    $query = $pdo->prepare($sql);

    return $query->execute([
        'id' => $id,
        'name' => $name,
        'surname' => $surname,
        'gender' => $gender,
        'phone' => $phone,
        'birthday' => $birthday,
        'education' => $education,
        'salary' => $salary,
        'idarbinimo_tipas' => $idarbinimoTipas,
        'pareigos_id' => $pareigosId,
    ]);
}

function deleteEmployee(PDO $pdo, int $id): bool
{
    $query = $pdo->prepare('DELETE FROM darbuotojai WHERE id=:id');
    return $query->execute(['id' => $id]);
}

function createPosition(PDO $pdo, string $name, string $salary): bool
{
    $sql = 'INSERT into pareigos(name, base_salary)
    VALUES (:name, :base_salary)';

    $query = $pdo->prepare($sql);
    return $query->execute([
        'name' => $name,
        'base_salary' => $salary,
    ]);
}

function deletePosition(PDO $pdo, int $id): bool
{
    $query = $pdo->prepare('DELETE FROM pareigos WHERE id=:id');
    return $query->execute(['id' => $id]);
}
