<?php require_once 'functions.php';?>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <div class="container">
    <a href="index.php">Atgal</a>
    <h1 text-align="center">Darbuotojo informacija</h1>
    <br>
    <table class="table">          
        <?php 
        $pdo = getConnection();    
        $id = (int) $_GET['id'];
        $employeeItem = getEmployee($pdo, $id);       
       // foreach ($resultEmployee as $employeeItem) {
            $positionName = getPositionName($pdo, (int) $employeeItem['pareigos_id']);?>  
        <tr>
          <th>Vardas ir pavardė</th>
          <td scope="col"> <?php echo $employeeItem['name'] . ' ' . $employeeItem['surname'];?></td>
        </tr>       
        <tr>
          <th>Telefonas</th>
          <td scope="col"> <?php echo $employeeItem['phone'];?></td>
        </tr>
        <tr>
          <th>Gimimo data</th>
          <td scope="col"> <?php echo $employeeItem['birthday'];?></td>
        </tr>
        <tr>  
          <th>Išsilavinimas</th>
          <td scope="col"> <?php echo $employeeItem['education'];?></td>
        </tr>
        <tr>
          <th>Atlyginimas</th>
          <td scope="col"> <?php echo $employeeItem['salary'];?></td>
        </tr>
        <tr>
          <th>Įdarbinimo tipas</th>
          <td scope="col"> <?php echo $employeeItem['idarbinimo_tipas'];?></td>
        </tr>
        <tr>
          <th>Pareigos</th>
          <td scope="col"> <?php echo $positionName['name'];?></td> 
        </tr>               
    </table>
    <br>
    <br>
  
    <h4>Darbdavio sanaudos:</h4>
    <?php $taxesForEmployee = getTaxesForEmployee($employeeItem['salary']);?>
    <table class=table>
        <tr>      
          <th>Pajamų mokestis</th>
          <td scope="col"><?php echo $taxesForEmployee['income_tax'];?></td>
        </tr>
        <tr>
          <th>Sveikatos draudimas</th>
          <td scope="col"> <?php echo $taxesForEmployee['health_security_tax'];?></td>
        </tr>       
        <tr>
          <th>Soc. draudimas</th>
          <td scope="col"> <?php echo $taxesForEmployee['social_security_tax'];?></td>
        </tr>
        <tr>
          <th>Atlyginimas atskaičius mokesčius (į rankas)</th>
          <td scope="col"> <?php echo $taxesForEmployee['salary_after_taxes'];?></td>
        </tr>
        <tr>  
          <th>Įmokos SODRAI</th>
          <td scope="col"> <?php echo $taxesForEmployee['SODRA'];?></td>
        </tr>
        <tr>
          <th>Garantinis fondas</th>
          <td scope="col"> <?php echo $taxesForEmployee['fund'];?></td>
        </tr>
        <tr>
          <th>Visi darbdavio sumokami mokesčiai</th>
          <td scope="col"> <?php echo $taxesForEmployee['total'];?></td>
        </tr>          
        <?php //} ?>  
    </table>
    <form action="editEmployee.php" method="POST">
        <input type="submit" value="Atnaujinti">
        <input type="hidden" value="<?php echo $employeeItem['id']; ?>" name="id">               
    </form>
</body>
</html>
